//Comentar
/*
Faça um programa que receba três notas, calcule e mostre a
média aritmética entre elas
*/

//Inicio
namespace exercicio_2{
    //Entrada de dados
    let nota1, nota2, nota3: number;

    nota1 = 7;
    nota2 = 8;
    nota3 = 3;

    let media: number;

    //Processar dados
    media = ((nota1 * nota2 * nota3) / 3);

    //Saida
    console.log("A media aritimetica é: " + media);
}
