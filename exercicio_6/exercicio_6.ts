//Comentar
/*Faça um programa que receba o salário-base de um
funcionário, calcule e mostre o salário a receber, sabendo-se
que esse funcionário tem gratificação de 5% sobre o salário
base e paga imposto de 7% sobre o salário-base
*/

//Inicio
namespace exercicio_6{
    //Entrada de dados

    let sal: number;
    let gratificacao: number;
    let imposto: number;


    sal = 1000;
    gratificacao = sal * 5 / 100; 
    imposto = sal * 7 / 100; 

    let novo_sal: number;

    //Processar dados
    gratificacao = (sal + (sal * gratificacao));
    console.log(`A gratificacao é ${gratificacao}`);

    imposto = (sal + (sal * imposto));
    console.log(`O imposto é ${imposto}`);

    novo_sal = (sal + gratificacao - imposto);

    //Saida
    console.log(`O novo salario é ${novo_sal}`);


    novo_sal = sal + gratificacao - imposto;

}