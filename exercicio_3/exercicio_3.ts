//Comentar
/*
Faça um programa que receba três notas, calcule e mostre a
média ponderada entre elas.
*/

//Inicio
namespace exercicio_3 {
    //Entrada de dados
    let nota1, peso1, nota2, peso2, nota3, peso3: number;

    nota1 = 10;
    nota2 = 6;
    nota3 = 7;

    peso1 = 3;
    peso2 = 1;
    peso3 = 6;

    let media: number;

    media = ((nota1 * peso1 + nota2 * peso2 + nota3 * peso3) / (peso1 + peso2 + peso3));

    console.log(`A media ponderada é: ${media}`);
}